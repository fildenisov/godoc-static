module gitlab/fildenisov/godoc-static

go 1.16

require (
	github.com/PuerkitoBio/goquery v1.6.1
	github.com/yuin/goldmark v1.3.7
	golang.org/x/mod v0.4.2
	golang.org/x/net v0.0.0-20210510120150-4163338589ed
)
